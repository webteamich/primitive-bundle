<?php

namespace Truelab\Bundle\PrimitiveBundle\Test\String;

use Truelab\Bundle\PrimitiveBundle\String\EnglishPluralizer;
use Truelab\Bundle\PrimitiveBundle\String\Inflector;

class InflectorTest extends \PHPUnit_Framework_TestCase
{
    /** @var array $data */
    protected $data;

    /** @var Inflector $inflector */
    protected $inflector;

    public function setUp()
    {
        $this->inflector = new Inflector();
        $this->data = array(
            'myMethodName' => 'my_method_name',
            'otherTest' => 'other_test',
            'veryLongStringTest' => 'very_long_string_test'
        );
    }

    public function testUnderscoreToCamelCase()
    {
        foreach ($this->data as $key=>$value) {
            $this->assertEquals($key, $this->inflector->underscoreToCamelCase($value));
        }
    }

    public function testDepluralize()
    {
        foreach ($this->data as $key=>$value) {
            $this->assertEquals($value, $this->inflector->camelCaseToUnderscore($key));
        }
    }

    public function testCapitalizeUnderscoreToCamelCase()
    {
        $data = array(
            'CapitalizedClassName'=>'capitalized_class_name',
            'OtherTest'=>'other_test'
        );
        foreach ($data as $key=>$value) {
            $this->assertEquals($key, $this->inflector->capitalizeUnderscoreToCamelCase($value));
        }
    }

}