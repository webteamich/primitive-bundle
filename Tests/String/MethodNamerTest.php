<?php

namespace Truelab\Bundle\PrimitiveBundle\Test\String;

use Truelab\Bundle\PrimitiveBundle\String\EnglishPluralizer;
use Truelab\Bundle\PrimitiveBundle\String\Inflector;
use Truelab\Bundle\PrimitiveBundle\String\MethodNamer;

class MethodNamerTest extends \PHPUnit_Framework_TestCase
{
    /** @var MethodNamer $methodNamer */
    protected $methodNamer;

    public function setUp()
    {
        $inflector = new Inflector();
        $pluralizer = new EnglishPluralizer();
        $this->methodNamer = new MethodNamer($inflector, $pluralizer);
    }

    public function testGetSetMethodFromUnderscorePlural()
    {
        $this->assertEquals('setPage', $this->methodNamer->getSetMethodFromUnderscorePlural('pages'));
    }

    public function testGetSetMethodFromUnderscoreSingular()
    {
        $this->assertEquals('setPage', $this->methodNamer->getSetMethodFromUnderscoreSingular('page'));
    }

    public function testGetAddMethodFromUnderscoreSingular()
    {
        $this->assertEquals('addPage', $this->methodNamer->getAddMethodFromUnderscoreSingular('page'));
    }

    public function testGetAddMethodFromUnderscorePlural()
    {
        $this->assertEquals('addPage', $this->methodNamer->getAddMethodFromUnderscorePlural('pages'));
    }
}