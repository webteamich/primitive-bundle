<?php

namespace Truelab\Bundle\PrimitiveBundle\Test\String;

use Truelab\Bundle\PrimitiveBundle\String\EnglishPluralizer;

class EnglishPluralizerTest extends \PHPUnit_Framework_TestCase
{
    /** @var array $data */
    protected $data;
    /** @var EnglishPluralizer $pluralizer */
    protected $pluralizer;

    public function setUp()
    {
        $this->pluralizer = new EnglishPluralizer();
        $this->data = array(
            'child' => 'children',
            'quiz' => 'quizzes',
            'user' => 'users',
            'page' => 'pages',
            'person' => 'people',
            'doctor' => 'doctors',
            'cry' => 'cries',
            'city' => 'cities',
            'foot' => 'feet',
            'tomato'=> 'tomatoes',
            'ox'=>'oxen',
            'sheep' => 'sheep',
            'fish' => 'fish',
            'shoe'=>'shoes',
            'bus'=>'buses'
        );
    }

    public function testPluralize()
    {
        foreach ($this->data as $key=>$value) {
            $this->assertEquals($value, $this->pluralizer->pluralize($key));
        }
    }

    public function testDepluralize()
    {
        $this->data['zzzz'] = 'zzzz';
        foreach ($this->data as $key=>$value) {
            $this->assertEquals($key, $this->pluralizer->depluralize($value));
        }
    }

}