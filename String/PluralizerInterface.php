<?php

namespace Truelab\Bundle\PrimitiveBundle\String;

interface PluralizerInterface
{
    /**
     * @param string $string
     * @return string
     */
    public function pluralize($string);

    /**
     * @param string $string
     * @return string
     */
    public function depluralize($string);

}