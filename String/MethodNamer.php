<?php

namespace Truelab\Bundle\PrimitiveBundle\String;

class MethodNamer
{
    /** @var Inflector $inflector */
    protected $inflector;
    /** @var PluralizerInterface $pluralizer */
    protected $pluralizer;

    public function __construct(Inflector $inflector, PluralizerInterface $pluralizer)
    {
        $this->inflector = $inflector;
        $this->pluralizer = $pluralizer;
    }

    public function getSetMethodFromUnderscorePlural($string)
    {
        return 'set' . $this->getSuffixMethodFromUnderscorePlural($string);
    }

    public function getAddMethodFromUnderscorePlural($string)
    {
        return 'add' . $this->getSuffixMethodFromUnderscorePlural($string);
    }

    public function getSetMethodFromUnderscoreSingular($string)
    {
        return 'set' . $this->getSuffixMethodFromUnderscoreSingular($string);
    }

    public function getAddMethodFromUnderscoreSingular($string)
    {
        return 'add' . $this->getSuffixMethodFromUnderscoreSingular($string);
    }

    protected function getSuffixMethodFromUnderscorePlural($string)
    {
        $singular = $this->pluralizer->depluralize($string);

        return $this->inflector->capitalizeUnderscoreToCamelCase($singular);
    }

    protected function getSuffixMethodFromUnderscoreSingular($string)
    {
        return $this->inflector->capitalizeUnderscoreToCamelCase($string);
    }

}