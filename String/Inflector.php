<?php

namespace Truelab\Bundle\PrimitiveBundle\String;

class Inflector
{
    /**
     * @param $string
     * @return string
     */
    public function underscoreToCamelCase($string)
    {
        $func = create_function('$c', 'return strtoupper($c[1]);');

        return preg_replace_callback('/_([a-z])/', $func, $string);
    }

    /**
     * @param $string
     * @return string
     */
    public function capitalizeUnderscoreToCamelCase($string)
    {
        $string[0] = strtoupper($string[0]);
        return $this->underscoreToCamelCase($string);
    }

    /**
     * @param $string
     * @return string
     */
    public function camelCaseToUnderscore($string)
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $string));
    }
}