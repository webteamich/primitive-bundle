<?php

namespace Truelab\Bundle\PrimitiveBundle\Arrays;


class Splitter
{
    public function getColumns($array, $columnCount)
    {
        $columns = array();
        for ($i = 0; $i<$columnCount; $i++) {
            $columns[$i] = array();
        }

        for ($i = $columnCount; ($i-$columnCount)<count($array); $i++) {
            $columns[$i%$columnCount][] = $array[$i-$columnCount];
        }
        return $columns;
    }
}